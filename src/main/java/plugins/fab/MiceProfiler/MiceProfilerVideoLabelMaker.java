/*
 * Copyright 2011, 2012 Institut Pasteur.
 *
 * This file is part of MiceProfiler.
 *
 * MiceProfiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiceProfiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiceProfiler. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.fab.MiceProfiler;

import icy.plugin.abstract_.PluginActionable;

/**
 * Just for backward compatibility (plugin is now bundled in Mice Profiler Tracker)
 */
public class MiceProfilerVideoLabelMaker extends PluginActionable
{
    final MiceProfilerVideoLabelMakerInternal implementer;

    public MiceProfilerVideoLabelMaker()
    {
        super();

        implementer = new MiceProfilerVideoLabelMakerInternal();
    }

    @Override
    public void run()
    {
        implementer.run();
    }
}