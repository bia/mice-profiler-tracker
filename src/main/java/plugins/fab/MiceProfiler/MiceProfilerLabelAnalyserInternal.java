/*
 * Copyright 2011, 2012 Institut Pasteur.
 * 
 * This file is part of MiceProfiler.
 * 
 * MiceProfiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MiceProfiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MiceProfiler. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.fab.MiceProfiler;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.DeviationRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.YIntervalSeries;
import org.jfree.data.xy.YIntervalSeriesCollection;

import icy.file.xls.XlsManager;
import icy.gui.dialog.ActionDialog;
import icy.gui.dialog.MessageDialog;
import icy.gui.dialog.SaveDialog;
import icy.gui.frame.IcyFrame;
import icy.gui.util.GuiUtil;
import icy.main.Icy;
import icy.swimmingPool.SwimmingObject;
import icy.swimmingPool.SwimmingPoolEvent;
import icy.swimmingPool.SwimmingPoolListener;
import jxl.format.Colour;

/**
 * @author Fab
 *         Goal : extract graphs from animal pool.
 *         Take its source from the swimming pool.
 *         Can be multi instanced.
 */
public class MiceProfilerLabelAnalyserInternal implements SwimmingPoolListener, ActionListener
{

    JPanel mainPanel = GuiUtil.generatePanel();
    IcyFrame icyFrame = GuiUtil.generateTitleFrame("Label Analyser", mainPanel, new Dimension(0, 0), true, true, true,
            true);

    ArrayList<EventSelector> eventSelectorList = null;
    JFreeChart chart;
    XYSeriesCollection xyDataset = new XYSeriesCollection();
    YIntervalSeriesCollection yintervalseriescollection = new YIntervalSeriesCollection();

    JTextField binSizeTextField = new JTextField("150");
    JTextField fpsTextField = new JTextField("15");
    JTextField totalTimeTextField = new JTextField("7200");
    ArrayList<AnimalPoolSelector> animalList = new ArrayList<AnimalPoolSelector>();
    IcyFrame graphFrame = new IcyFrame("graph", true, true, true, true);
    // JCheckBox displayLegendCheckBox = new JCheckBox("display Legend", false);
    JCheckBoxMenuItem displayLegendCheckBoxMenuItem = new JCheckBoxMenuItem("display Legend", true);

    // JButton extractDataToProbabilityGridButton = new JButton("Compute decision graph script");
    JMenuItem extractDataToProbabilityGridMenuItem = new JMenuItem("Compute decision graph script to console");
    JTextField extractDataToProbabilityTimeValue = new JTextField("150");
    // JButton extractCurrentDataGraphToExcelButton = new JButton("Export current graph to xls");

    JMenuItem extractCurrentDataGraphToExcelMenuItem = new JMenuItem("Export current graph to xls");
    JMenuItem extractCurrentDataGraphWithAllLabelDensityToExcelMenuItem = new JMenuItem(
            "Export graphs to xls (one per label-density)");
    // JButton extractAllDataToGraphAndExcel = new JButton("Export all datas (in pools) to xls");
    JMenuItem extractAllDataToGraphAndExcelMenuItem = new JMenuItem("Export all datas (in pools) to xls");

    public MiceProfilerLabelAnalyserInternal()
    {
        // CHECK IF ANIMAL ARE LOADED IN SWIMMING POOL. IF NOT RETURN.

        animalList.clear();
        ArrayList<SwimmingObject> liste = Icy.getMainInterface().getSwimmingPool().getObjects();
        for (SwimmingObject so : liste)
        {
            if (so.getObject() instanceof Animal)
            {
                animalList.add(new AnimalPoolSelector((Animal) so.getObject()));
            }
        }

        if (animalList.size() == 0)
        {
            MessageDialog.showDialog("No animal loaded, please run VideoLabelMaker first.",
                    MessageDialog.INFORMATION_MESSAGE);
            return;
        }

        // END OF ANIMAL LOADED CHECK

        Icy.getMainInterface().getSwimmingPool().addListener(this);

        chart = ChartFactory.createXYLineChart("", "t", "y", yintervalseriescollection, PlotOrientation.VERTICAL, true, // true ,
                false, false);

        XYPlot xyplot = (XYPlot) chart.getPlot();
        xyplot.setInsets(new org.jfree.chart.ui.RectangleInsets(5D, 5D, 5D, 20D));
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setAxisOffset(new org.jfree.chart.ui.RectangleInsets(5D, 5D, 5D, 5D));
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);
        DeviationRenderer deviationrenderer = new DeviationRenderer(true, false);
        for (int i = 0; i < 100; i++)
        {
            deviationrenderer.setSeriesStroke(i, new BasicStroke(3F, 1, 1));
            deviationrenderer.setSeriesStroke(i + 1, new BasicStroke(3F, 1, 1));
            deviationrenderer.setSeriesFillPaint(0, new Color(255, 200, 200));
            deviationrenderer.setSeriesFillPaint(1, new Color(200, 200, 255));
            deviationrenderer.setSeriesFillPaint(3, new Color(200, 255, 200));
            deviationrenderer.setSeriesFillPaint(2, new Color(255, 255, 200));
        }
        xyplot.setRenderer(deviationrenderer);

        refreshData();

        icyFrame.addToDesktopPane();
        icyFrame.pack();
        icyFrame.setSize(800, 600);
        icyFrame.center();
        icyFrame.toFront();
        icyFrame.setVisible(true);

        graphFrame.addToDesktopPane();
        // graphFrame.center();
        graphFrame.setVisible(true);
        icyFrame.toFront();

        extractCurrentDataGraphToExcelMenuItem.addActionListener(this);
        extractAllDataToGraphAndExcelMenuItem.addActionListener(this);
        // displayLegendCheckBoxMenuItem.addActionListener( this );
        extractDataToProbabilityGridMenuItem.addActionListener(this);
        extractCurrentDataGraphWithAllLabelDensityToExcelMenuItem.addActionListener(this);

        JMenuBar menuBar = new JMenuBar();
        JMenu menuExport = new JMenu("Export");
        menuExport.add(extractCurrentDataGraphToExcelMenuItem);
        menuExport.add(extractAllDataToGraphAndExcelMenuItem);
        menuExport.add(extractDataToProbabilityGridMenuItem);
        menuExport.add(extractCurrentDataGraphWithAllLabelDensityToExcelMenuItem);
        menuBar.add(menuExport);

        // JMenu menuGraph = new JMenu("Graph");
        // menuGraph.add( displayLegendCheckBoxMenuItem );
        // menuBar.add( menuGraph );

        icyFrame.setJMenuBar(menuBar);

    }

    public void run()
    {
        // nothing here
    }

    /**
     * refresh graphs
     * 
     * @param file
     */
    void refreshGraph(boolean xlsExport, String file)
    {

        XlsManager xls = null;
        int cursorX = 0;
        int cursorY = 1;

        if (xlsExport)
        {
            String saveFileName = file;
            if (file == null)
            {
                saveFileName = SaveDialog.chooseFile("Export current graph to xls file",
                        System.getProperty("user.home"), "outputGraph.xls", ".xls");
                if (saveFileName == null)
                    return;
            }

            try
            {
                xls = new XlsManager(new File(saveFileName));
                xls.createNewPage("Mice profiler graph export");

            }
            catch (IOException e)
            {

                e.printStackTrace();
            }
        }

        int binSize = Integer.parseInt(binSizeTextField.getText());
        int endFrame = Integer.parseInt(totalTimeTextField.getText());
        float fps = Float.parseFloat(fpsTextField.getText());

        xyDataset.removeAllSeries();
        yintervalseriescollection.removeAllSeries();

        // creation des series:

        // no pool

        for (EventSelector eventSelector : eventSelectorList)
        {

            for (AnimalPoolSelector animalSelector : animalList)
            {

                if (animalSelector.noPool.isSelected())
                {

                    if (animalSelector.enable.isSelected())
                    {

                        if (eventSelector.nbEventCheckBox.isSelected())
                        {
                            EventTimeLine eventTimeLine = animalSelector.animal.eventTimeLineList
                                    .get(eventSelector.eventNumber);
                            String titreSerie = animalSelector.animal.animalName + " np Event Nb"
                                    + eventTimeLine.criteriaName;
                            XYSeries seriesXY = new XYSeries(titreSerie);

                            if (xlsExport)
                            {
                                cursorX++;
                                xls.setLabel(cursorX, 0, titreSerie);
                                cursorY = 1;
                            }

                            YIntervalSeries yintervalseries = new YIntervalSeries(
                                    animalSelector.animal.animalName + " np Event Nb" + eventTimeLine.criteriaName);

                            for (int t = 0; t < endFrame; t += binSize)
                            {
                                double value = eventTimeLine.getNbEvent(t, t + binSize - 1);
                                yintervalseries.add(t / fps, value, value, value);

                                if (xlsExport)
                                {
                                    xls.setNumber(0, cursorY, t / fps); // legende
                                    xls.setNumber(cursorX, cursorY, value); // valeur
                                    cursorY++;
                                }

                            }
                            yintervalseriescollection.addSeries(yintervalseries);

                        }

                        if (eventSelector.lengthCheckBox.isSelected())
                        {
                            EventTimeLine eventTimeLine = animalSelector.animal.eventTimeLineList
                                    .get(eventSelector.eventNumber);
                            XYSeries seriesXY = new XYSeries(
                                    animalSelector.animal.animalName + " np Event l" + eventTimeLine.criteriaName);

                            String titreSerie = animalSelector.animal.animalName + " np Event l"
                                    + eventTimeLine.criteriaName;
                            YIntervalSeries yintervalseries = new YIntervalSeries(titreSerie);

                            if (xlsExport)
                            {
                                cursorX++;
                                xls.setLabel(cursorX, 0, titreSerie);
                                cursorY = 1;
                            }

                            for (int t = 0; t < endFrame; t += binSize)
                            {
                                double value = eventTimeLine.getLengthEvent(t, t + binSize - 1);
                                yintervalseries.add(t / fps, value, value, value);

                                if (xlsExport)
                                {
                                    xls.setNumber(0, cursorY, t / fps); // legende
                                    xls.setNumber(cursorX, cursorY, value); // valeur
                                    cursorY++;
                                }

                            }
                            yintervalseriescollection.addSeries(yintervalseries);

                        }

                        // critere discret
                        if (eventSelector.displayDiscreteValueCheckBox.isSelected())
                        {
                            EventTimeLine eventTimeLine = animalSelector.animal.eventTimeLineList
                                    .get(eventSelector.eventNumber);
                            XYSeries seriesXY = new XYSeries(
                                    animalSelector.animal.animalName + " dv Event " + eventTimeLine.criteriaName);

                            String titreSerie = animalSelector.animal.animalName + " dv Event "
                                    + eventTimeLine.criteriaName;

                            YIntervalSeries yintervalseries = new YIntervalSeries(titreSerie);

                            if (xlsExport)
                            {
                                cursorX++;
                                xls.setLabel(cursorX, 0, titreSerie);
                                cursorY = 1;
                            }

                            for (int t = 0; t < endFrame; t += binSize)
                            {

                                double value = eventTimeLine.getMeanValue(t, t + binSize - 1);
                                yintervalseries.add(t / fps, value, value, value);

                                if (xlsExport)
                                {
                                    xls.setNumber(0, cursorY, t / fps); // legende
                                    xls.setNumber(cursorX, cursorY, value); // valeur
                                    cursorY++;
                                }

                            }
                            yintervalseriescollection.addSeries(yintervalseries);

                        }

                        // critere density
                        if (eventSelector.densityCheckBox.isSelected())
                        {

                            EventTimeLine eventTimeLine = animalSelector.animal.eventTimeLineList
                                    .get(eventSelector.eventNumber);
                            XYSeries seriesXY = new XYSeries(
                                    animalSelector.animal.animalName + " dv Density " + eventTimeLine.criteriaName);

                            String titreSerie = animalSelector.animal.animalName + " dv Density "
                                    + eventTimeLine.criteriaName;

                            YIntervalSeries yintervalseries = new YIntervalSeries(titreSerie);

                            if (xlsExport)
                            {
                                cursorX++;
                                xls.setLabel(cursorX, 0, titreSerie);
                                cursorY = 1;
                            }

                            for (int t = 0; t < endFrame; t += binSize)
                            {

                                double value = eventTimeLine.getDensity(t, t + binSize - 1);

                                yintervalseries.add(t / fps, value, value, value);

                                if (xlsExport)
                                {
                                    xls.setNumber(0, cursorY, t / fps); // legende
                                    xls.setNumber(cursorX, cursorY, value); // valeur
                                    cursorY++;
                                }

                            }
                            yintervalseriescollection.addSeries(yintervalseries);

                        }

                    }
                }
            }

        }

        updateGraphSeriesForPool(Criteria.NB_EVENT, 1, endFrame, binSize, xls, xlsExport);
        updateGraphSeriesForPool(Criteria.LENGTH_EVENT, 1, endFrame, binSize, xls, xlsExport);
        updateGraphSeriesForPool(Criteria.DISCRETE_VALUE, 1, endFrame, binSize, xls, xlsExport);
        updateGraphSeriesForPool(Criteria.DENSITY_VALUE, 1, endFrame, binSize, xls, xlsExport);
        updateGraphSeriesForPool(Criteria.NB_EVENT, 2, endFrame, binSize, xls, xlsExport);
        updateGraphSeriesForPool(Criteria.LENGTH_EVENT, 2, endFrame, binSize, xls, xlsExport);
        updateGraphSeriesForPool(Criteria.DISCRETE_VALUE, 2, endFrame, binSize, xls, xlsExport);
        updateGraphSeriesForPool(Criteria.DENSITY_VALUE, 2, endFrame, binSize, xls, xlsExport);

        if (xlsExport)
        {
            // System.out.println( "xls debug: " + xls.getExcelPage() );
            xls.SaveAndClose();
        }

    }

    enum Criteria
    {
        NB_EVENT, LENGTH_EVENT, DISCRETE_VALUE, DENSITY_VALUE
    }

    void updateGraphSeriesForPool(Criteria criteria, int pool, int endFrame, int binSize, XlsManager xls,
            boolean xlsExport)
    {
        int cursorX = 1;
        int cursorY = 0;

        float fps = Float.parseFloat(fpsTextField.getText());

        boolean process = false;
        for (AnimalPoolSelector animalSelector : animalList)
        {
            if ((pool == 1 && animalSelector.pool1.isSelected()) || (pool == 2 && animalSelector.pool2.isSelected()))
            {
                process = true;
            }
        }

        if (!process)
            return;

        for (EventSelector eventSelector : eventSelectorList)
        {
            if ((criteria == Criteria.NB_EVENT && eventSelector.nbEventCheckBox.isSelected())
                    || (criteria == Criteria.LENGTH_EVENT && eventSelector.lengthCheckBox.isSelected())
                    || (criteria == Criteria.DISCRETE_VALUE && eventSelector.displayDiscreteValueCheckBox.isSelected())
                    || (criteria == Criteria.DENSITY_VALUE && eventSelector.densityCheckBox.isSelected()))
            {

                String chaine = "p" + pool + " " + criteria.toString();
                chaine += animalList.get(0).animal.eventTimeLineList.get(eventSelector.eventNumber).criteriaName;
                XYSeries seriesXY = new XYSeries(chaine);
                YIntervalSeries yintervalseries = new YIntervalSeries(chaine);

                if (xlsExport)
                {
                    xls.createNewPage(chaine);
                    cursorX = 1;
                }

                for (int t = 0; t < endFrame; t += binSize)
                {
                    if (xlsExport)
                    {
                        xls.setLabel(cursorX, cursorY, "" + t + "-" + (t + binSize));
                        cursorY++;
                        cursorY++;

                    }

                    // ROI3DArea r = new ROI3DArea( new Point3D.Double( 0,0,0 ) );

                    ArrayList<Double> listResultat = new ArrayList<Double>();

                    for (AnimalPoolSelector animalSelector : animalList)
                    {

                        if ((pool == 1 && animalSelector.pool1.isSelected())
                                || (pool == 2 && animalSelector.pool2.isSelected()))
                        {

                            if (animalSelector.enable.isSelected())
                            {
                                EventTimeLine eventTimeLine = animalSelector.animal.eventTimeLineList
                                        .get(eventSelector.eventNumber);

                                if (criteria == Criteria.NB_EVENT)
                                {
                                    listResultat.add(eventTimeLine.getNbEvent(t, t + binSize - 1));

                                    if (xlsExport)
                                    {
                                        xls.setNumber(cursorX, cursorY, eventTimeLine.getNbEvent(t, t + binSize - 1));
                                        cursorY++;
                                    }

                                }
                                if (criteria == Criteria.LENGTH_EVENT)
                                {
                                    listResultat.add(eventTimeLine.getLengthEvent(t, t + binSize - 1));

                                    if (xlsExport)
                                    {
                                        xls.setNumber(cursorX, cursorY,
                                                eventTimeLine.getLengthEvent(t, t + binSize - 1));
                                        cursorY++;
                                    }

                                }

                                if (criteria == Criteria.DISCRETE_VALUE)
                                {
                                    listResultat.add(eventTimeLine.getMeanValue(t, t + binSize - 1));

                                    if (xlsExport)
                                    {
                                        xls.setNumber(cursorX, cursorY, eventTimeLine.getMeanValue(t, t + binSize - 1));
                                        cursorY++;
                                    }

                                }
                                if (criteria == Criteria.DENSITY_VALUE)
                                {
                                    listResultat.add(eventTimeLine.getDensity(t, t + binSize - 1));

                                    if (xlsExport)
                                    {
                                        xls.setNumber(cursorX, cursorY, eventTimeLine.getDensity(t, t + binSize - 1));
                                        cursorY++;
                                    }

                                }

                            }
                        }
                    }

                    if (listResultat.size() != 0)
                    {
                        double[] resultatTab2 = new double[listResultat.size()];
                        for (int i = 0; i < listResultat.size(); i++)
                        {
                            resultatTab2[i] = listResultat.get(i);
                        }

                        double error = flanagan.analysis.Stat.standardDeviation(resultatTab2);
                        error = error / Math.sqrt(listResultat.size() - 1);
                        double mean = flanagan.analysis.Stat.mean(resultatTab2);

                        yintervalseries.add(t / fps, mean, mean - (error), mean + (error));

                        if (xlsExport)
                        {
                            cursorY++;
                            xls.setNumber(cursorX, cursorY, mean);
                        }

                    }
                    else
                    {
                        yintervalseries.add(t / fps, 0, 0, 0);
                    }

                    cursorX++;
                    cursorY = 0;
                }

                xyDataset.addSeries(seriesXY);

                yintervalseriescollection.addSeries(yintervalseries);

            }
        }
    }

    /**
     * Refresh Datas
     */
    private void refreshData()
    {

        mainPanel.removeAll();

        JPanel animalPoolListPanel = GuiUtil.generatePanel("Animal pool List");
        JPanel criteriaListPanel = GuiUtil.generatePanel("criteria List");

        ArrayList<SwimmingObject> liste = Icy.getMainInterface().getSwimmingPool().getObjects();
        animalList.clear();

        Animal animalToGetCriteria = null;
        for (SwimmingObject so : liste)
        {
            if (so.getObject() instanceof Animal)
            {
                animalList.add(new AnimalPoolSelector((Animal) so.getObject()));
                animalToGetCriteria = (Animal) so.getObject();
            }
        }

        if (animalToGetCriteria == null) // there is no more animal in pool
        {
            return;
        }

        for (AnimalPoolSelector animalSelector : animalList)
        {
            animalPoolListPanel.add(animalSelector.panel);
        }

        mainPanel.add(GuiUtil.besidesPanel(new JLabel("Bin Size ( nb frames )"), binSizeTextField,
                new JLabel("Total time to compute ( nb frames )"), totalTimeTextField));
        // mainPanel.add( GuiUtil.besidesPanel ( displayLegendCheckBox , extractAllDataToGraphAndExcelMenuItem , extractDataToProbabilityGridButton ) );
        // mainPanel.add( GuiUtil.besidesPanel ( displayLegendCheckBox , extractDataToProbabilityGridMenuItem ) );
        // mainPanel.add( GuiUtil.besidesPanel ( displayLegendCheckBoxMenuItem ) );

        // mainPanel.add( GuiUtil.besidesPanel( new JLabel("time window forward:") , extractDataToProbabilityTimeValue , extractCurrentDataGraphToExcelMenuItem
        // ) );
        mainPanel.add(GuiUtil.besidesPanel(new JLabel("time window forward:"), extractDataToProbabilityTimeValue));
        mainPanel.add(GuiUtil.besidesPanel(new JLabel("FPS:"), fpsTextField));

        eventSelectorList = new ArrayList<EventSelector>();

        for (EventTimeLine et : animalToGetCriteria.eventTimeLineList)
        {
            EventSelector eventSelector = new EventSelector(animalToGetCriteria.eventTimeLineList.indexOf(et), et);
            eventSelectorList.add(eventSelector);

            criteriaListPanel.add(eventSelector.panel);
        }

        JPanel parameterPanel = new JPanel(new BorderLayout());

        JScrollPane animalPoolListScrollPane = new JScrollPane(animalPoolListPanel,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        JScrollPane criteriaListScrollPane = new JScrollPane(criteriaListPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        parameterPanel.add(animalPoolListScrollPane, BorderLayout.WEST);
        parameterPanel.add(criteriaListScrollPane, BorderLayout.CENTER);

        mainPanel.add(parameterPanel);

        graphFrame.getContentPane()
                .add(new ChartPanel(chart, 700, 200, 700, 200, 700, 700, false, false, true, true, true, true));
        graphFrame.pack();

        refreshGraph(false, "");

        mainPanel.updateUI();

    }

    @Override
    public void swimmingPoolChangeEvent(SwimmingPoolEvent swimmingPoolEvent)
    {

        refreshData();

    }

    class EventSelector implements ActionListener
    {
        JPanel panel = GuiUtil.generatePanelWithoutBorder();
        int eventNumber;
        JCheckBox nbEventCheckBox = new JCheckBox("nb Event");
        JCheckBox lengthCheckBox = new JCheckBox("length");
        JCheckBox densityCheckBox = new JCheckBox("density");
        JLabel criteriaLabel;
        JCheckBox displayDiscreteValueCheckBox = new JCheckBox("discrete Value");

        public EventSelector(int eventNumber, EventTimeLine e)
        {
            this.eventNumber = eventNumber;

            int maxSizeString = e.criteriaName.length();
            if (maxSizeString > 40)
                maxSizeString = 40;

            criteriaLabel = new JLabel(e.criteriaName.substring(0, maxSizeString));
            criteriaLabel.setToolTipText(e.criteriaName);

            if (e.timeLineCategory == TimeLineCategory.USE_BOOLEAN_EVENT)
            {
                panel.add(GuiUtil.besidesPanel(criteriaLabel, densityCheckBox, nbEventCheckBox, lengthCheckBox));
            }

            if (e.timeLineCategory == TimeLineCategory.USE_DISCRETE_EVENT)
            {
                panel.add(GuiUtil.besidesPanel(criteriaLabel, displayDiscreteValueCheckBox));
            }

            densityCheckBox.addActionListener(this);
            nbEventCheckBox.addActionListener(this);
            lengthCheckBox.addActionListener(this);
            displayDiscreteValueCheckBox.addActionListener(this);

        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            refreshGraph(false, "");
        }

    }

    class AnimalPoolSelector implements ActionListener
    {
        JPanel panel = GuiUtil.generatePanelWithoutBorder();
        Animal animal;

        JCheckBox enable = new JCheckBox("Enable", true);
        JRadioButton pool1 = new JRadioButton("Pool 1");
        JRadioButton pool2 = new JRadioButton("Pool 2");
        JRadioButton noPool = new JRadioButton("no pool", true);

        ButtonGroup poolGroup = new ButtonGroup();
        ButtonGroup carecterizationGroup = new ButtonGroup();

        public AnimalPoolSelector(Animal animal)
        {

            this.animal = animal;

            enable.addActionListener(this);
            pool1.addActionListener(this);
            pool2.addActionListener(this);
            noPool.addActionListener(this);

            poolGroup.add(pool1);
            poolGroup.add(pool2);
            poolGroup.add(noPool);

            int maxSizeString = animal.animalName.length();
            if (maxSizeString > 5)
                maxSizeString = 5;

            enable.setText(animal.animalName.substring(0, maxSizeString));
            enable.setToolTipText(animal.animalName);

            panel.add(GuiUtil.besidesPanel(new Component[] {

                enable, pool1, pool2, noPool}

            ));

        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            refreshGraph(false, "");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {

        refreshGraph(false, "");

        if (e.getSource() == extractCurrentDataGraphToExcelMenuItem)
        {
            extractCurrentDataGraphToExcel();
        }

        if (e.getSource() == extractAllDataToGraphAndExcelMenuItem)
        {
            exportAllData();
        }

        if (e.getSource() == extractDataToProbabilityGridMenuItem)
        {
            extractDataToProbabilityGrid2();
        }

        if (e.getSource() == extractCurrentDataGraphWithAllLabelDensityToExcelMenuItem)
        {
            extractDataGraphWithAllLabelDensityToExcel();
        }

    }

    /**
     * Extract all the data by selecting all the different criteria, one by one.
     */
    private void extractDataGraphWithAllLabelDensityToExcel()
    {
        String saveFileName = SaveDialog.chooseFile("Save xls file", "", "Mice Profiler - ");

        if (saveFileName == null)
            return;

        for (EventSelector eventSelector : eventSelectorList)
        {
            for (EventSelector eventSelector2 : eventSelectorList) // clear all check boxes
            {
                eventSelector2.densityCheckBox.setSelected(false);
            }

            eventSelector.densityCheckBox.setSelected(true);

            String eventLabel = eventSelector.criteriaLabel.getText().replaceAll("[^a-zA-Z0-9\\._]+", "_");
            String fileName = saveFileName + " - " + eventLabel + ".xls";

            refreshGraph(true, fileName);
        }

    }

    private void extractCurrentDataGraphToExcel()
    {

        refreshGraph(true, null);

    }

    /**
     * Extraction des grilles de proba de passage d'un etat a un autre sous feuille excel.
     * Nouvelle version 2 (nov 2010)
     */
    private void extractDataToProbabilityGrid2()
    {

        System.out.println("export version nov 2010");

        int timeWindow = Integer.parseInt(extractDataToProbabilityTimeValue.getText());
        int offsetY = 3;
        int offsetX = 1;

        // XlsManager xls =null;
        // try {
        // xls = new XlsManager( System.getProperty("user.home") + "\\output "+(timeWindow-15)+"-"+timeWindow+"f.xls");
        // } catch (IOException e) {
        //
        // e.printStackTrace();
        // }

        // JTextField startTextField = new JTextField("0");

        ActionDialog actionDialog = new ActionDialog("Select parameters for tansition graph.");
        JPanel mainPanel = new JPanel();
        actionDialog.setPreferredSize(new Dimension(400, 400));
        actionDialog.getMainPanel().add(new JScrollPane(mainPanel));
        // actionDialog.getMainPanel().setLayout( new BoxLayout( actionDialog.getMainPanel() , BoxLayout.PAGE_AXIS ));
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        animalList.get(0).animal.eventTimeLineList.size();

        JTextField minProbability = new JTextField("0.3");
        mainPanel.add(GuiUtil.createLineBoxPanel(new JLabel("Min probability: "), minProbability));
        JTextField fpsTextField = new JTextField("15");
        mainPanel.add(GuiUtil.createLineBoxPanel(new JLabel("FPS: "), fpsTextField));
        JTextField maxNumberOfCandidateTextField = new JTextField("4");
        mainPanel
                .add(GuiUtil.createLineBoxPanel(new JLabel("Max number of ancestor: "), maxNumberOfCandidateTextField));
        JCheckBox forceLocation = new JCheckBox("Force the location of the event in the graph (circular rendering)",
                false);
        mainPanel.add(GuiUtil.createLineBoxPanel(forceLocation));

        HashMap<Integer, JCheckBox> selectedEventHashMap = new HashMap<Integer, JCheckBox>();

        for (EventTimeLine event : animalList.get(0).animal.eventTimeLineList)
        {
            JCheckBox booleanField = new JCheckBox(event.criteriaName, true);
            // actionDialog.getMainPanel().add( GuiUtil.createLineBoxPanel( booleanField ) );
            mainPanel.add(GuiUtil.createLineBoxPanel(booleanField));
            int index = animalList.get(0).animal.eventTimeLineList.indexOf(event);
            selectedEventHashMap.put(index, booleanField);
            if (index == 1)
                booleanField.setSelected(false);
            if (index >= 7 && index <= 10)
                booleanField.setSelected(false);
            if (index >= 13 && index <= 20)
                booleanField.setSelected(false);
            if (index >= 27)
                booleanField.setSelected(false);

        }
        actionDialog.pack();
        actionDialog.setLocationRelativeTo(null);
        actionDialog.setVisible(true);

        if (actionDialog.isCanceled())
            return;

        float fps = Float.parseFloat(fpsTextField.getText());
        int MAX_NUMBER_OF_CANDIDATE = Integer.parseInt(maxNumberOfCandidateTextField.getText());

        System.out.println("WARNING:");
        System.out.println(
                "This output consider all the loaded animal in the Label Analyser as coming from the same pool.");
        System.out.println("i.e. only 1 pool is considered.");
        System.out.println("version Jan 2013. p>0.1 instead of p>0.3");

        int nbTotalDeCritere = animalList.get(0).animal.eventTimeLineList.size();

        float poolProba0_4[][][] = new float[animalList.size()][nbTotalDeCritere][nbTotalDeCritere];
        float poolProba4_8[][][] = new float[animalList.size()][nbTotalDeCritere][nbTotalDeCritere];

        for (int animalNumber = 0; animalNumber < animalList.size(); animalNumber++)
        {
            Animal animal = animalList.get(animalNumber).animal;
            // xls.createNewPage( animal.animalName );

            // xls.setLabel( 0 , 0 , "se lit: event de la colonne x a une proba p d etre precede par ligne y");
            // xls.setLabel( 0 , 1 , "timeforward(frames):" + (timeWindow-15) + " to " + timeWindow );
            // xls.setLabel( 0 , 2 , "animal:" + animal.animalName );

            // int FENETRE_RECHERCHE_FRAME = 15*3;
            int FENETRE_RECHERCHE_FRAME = (int) (fps * 3);

            for (int eventNumberSource = 0; eventNumberSource < animal.eventTimeLineList.size(); eventNumberSource++) // parcours la liste des events.
            {

                // xls.setNumber( 2*eventNumberSource+1 + offsetX, 0 + offsetY , eventNumberSource + 1 );

                for (int eventNumberTarget = 0; eventNumberTarget < animal.eventTimeLineList
                        .size(); eventNumberTarget++) // parcours la liste des events.
                {

                    // xls.setNumber( 0 + offsetX , eventNumberTarget+1 + offsetY , eventNumberTarget + 1 );
                    // xls.setLabel( 0 , eventNumberTarget+1 + offsetY , animalList.get( 0 ).animal.eventTimeLineList.get( eventNumberTarget ).criteriaName );

                    EventTimeLine eventTimeLineSource = animal.eventTimeLineList.get(eventNumberSource);
                    EventTimeLine eventTimeLineTarget = animal.eventTimeLineList.get(eventNumberTarget);

                    float nbEventSource04 = 0;
                    float nbEventTarget04 = 0;
                    float nbPassageSourceTargetOk04 = 0;
                    float nbPassageTargetSourceOk04 = 0;

                    for (EventCriteria eventCriteriaSource : eventTimeLineSource.eventList)
                    {
                        if (eventCriteriaSource.startFrame > 3600)
                            continue;
                        nbEventSource04++;

                        for (EventCriteria eventCriteriaTarget : eventTimeLineTarget.eventList)
                        {
                            if (eventCriteriaTarget.startFrame > 3600)
                                continue;

                            if (eventCriteriaTarget.startFrame >= eventCriteriaSource.endFrame
                                    && eventCriteriaTarget.startFrame <= eventCriteriaSource.endFrame
                                            + FENETRE_RECHERCHE_FRAME)
                            {
                                nbPassageSourceTargetOk04++;
                                break;
                            }
                        }
                    }

                    for (EventCriteria eventCriteriaTarget : eventTimeLineTarget.eventList)
                    {
                        if (eventCriteriaTarget.startFrame > 3600)
                            continue;
                        nbEventTarget04++;

                        for (EventCriteria eventCriteriaSource : eventTimeLineSource.eventList)
                        {
                            if (eventCriteriaSource.startFrame > 3600)
                                continue;
                            if (eventCriteriaTarget.startFrame >= eventCriteriaSource.endFrame
                                    && eventCriteriaTarget.startFrame <= eventCriteriaSource.endFrame
                                            + FENETRE_RECHERCHE_FRAME)
                            {
                                nbPassageTargetSourceOk04++;
                                break;
                            }
                        }
                    }

                    float probaStoT04 = nbPassageSourceTargetOk04 / nbEventSource04;
                    float probaTtoS04 = nbPassageTargetSourceOk04 / nbEventTarget04;
                    float proba2Way04 = probaStoT04 * probaTtoS04;

                    float nbEventSource48 = 0;
                    float nbEventTarget48 = 0;
                    float nbPassageSourceTargetOk48 = 0;
                    float nbPassageTargetSourceOk48 = 0;

                    for (EventCriteria eventCriteriaSource : eventTimeLineSource.eventList)
                    {
                        if (eventCriteriaSource.startFrame < 3600)
                            continue;
                        nbEventSource48++;

                        for (EventCriteria eventCriteriaTarget : eventTimeLineTarget.eventList)
                        {
                            if (eventCriteriaTarget.startFrame < 3600)
                                continue;

                            if (eventCriteriaTarget.startFrame >= eventCriteriaSource.endFrame
                                    && eventCriteriaTarget.startFrame <= eventCriteriaSource.endFrame
                                            + FENETRE_RECHERCHE_FRAME)
                            {
                                nbPassageSourceTargetOk48++;
                                break;
                            }
                        }
                    }

                    for (EventCriteria eventCriteriaTarget : eventTimeLineTarget.eventList)
                    {
                        if (eventCriteriaTarget.startFrame < 3600)
                            continue;
                        nbEventTarget48++;

                        for (EventCriteria eventCriteriaSource : eventTimeLineSource.eventList)
                        {
                            if (eventCriteriaSource.startFrame < 3600)
                                continue;
                            if (eventCriteriaTarget.startFrame >= eventCriteriaSource.endFrame
                                    && eventCriteriaTarget.startFrame <= eventCriteriaSource.endFrame
                                            + FENETRE_RECHERCHE_FRAME)
                            {
                                nbPassageTargetSourceOk48++;
                                break;
                            }
                        }
                    }

                    float probaStoT48 = nbPassageSourceTargetOk48 / nbEventSource48;
                    float probaTtoS48 = nbPassageTargetSourceOk48 / nbEventTarget48;
                    float proba2Way48 = probaStoT48 * probaTtoS48;

                    if (eventNumberSource != 9 && eventNumberSource != 10 && eventNumberTarget != 9
                            && eventNumberTarget != 10) // enleve le speed
                    {
                        poolProba0_4[animalNumber][eventNumberSource][eventNumberTarget] = proba2Way04;
                        poolProba4_8[animalNumber][eventNumberSource][eventNumberTarget] = proba2Way48;
                    }

                }
            }

        }

        float meanAnimalProba0_4[][] = new float[nbTotalDeCritere][nbTotalDeCritere];
        float stddevAnimalProba0_4[][] = new float[nbTotalDeCritere][nbTotalDeCritere];
        float meanAnimalProba4_8[][] = new float[nbTotalDeCritere][nbTotalDeCritere];
        float stddevAnimalProba4_8[][] = new float[nbTotalDeCritere][nbTotalDeCritere];

        for (int eventNumberSource = 0; eventNumberSource < nbTotalDeCritere; eventNumberSource++) // parcours la liste des events.
        {
            for (int eventNumberTarget = 0; eventNumberTarget < nbTotalDeCritere; eventNumberTarget++) // parcours la liste des events.
            {
                float[] tabProba = new float[animalList.size()];
                for (int animalNumber = 0; animalNumber < animalList.size(); animalNumber++)
                {
                    tabProba[animalNumber] = poolProba0_4[animalNumber][eventNumberSource][eventNumberTarget];
                    meanAnimalProba0_4[eventNumberSource][eventNumberTarget] += poolProba0_4[animalNumber][eventNumberSource][eventNumberTarget];

                }
                meanAnimalProba0_4[eventNumberSource][eventNumberTarget] /= animalList.size();
                stddevAnimalProba0_4[eventNumberSource][eventNumberTarget] = flanagan.analysis.Stat
                        .standardDeviation(tabProba);
                ;
            }
        }

        for (int eventNumberSource = 0; eventNumberSource < nbTotalDeCritere; eventNumberSource++) // parcours la liste des events.
        {
            for (int eventNumberTarget = 0; eventNumberTarget < nbTotalDeCritere; eventNumberTarget++) // parcours la liste des events.
            {
                float[] tabProba = new float[animalList.size()];
                for (int animalNumber = 0; animalNumber < animalList.size(); animalNumber++)
                {
                    tabProba[animalNumber] = poolProba4_8[animalNumber][eventNumberSource][eventNumberTarget];
                    meanAnimalProba4_8[eventNumberSource][eventNumberTarget] += poolProba4_8[animalNumber][eventNumberSource][eventNumberTarget];

                }
                meanAnimalProba4_8[eventNumberSource][eventNumberTarget] /= animalList.size();
                stddevAnimalProba4_8[eventNumberSource][eventNumberTarget] = flanagan.analysis.Stat
                        .standardDeviation(tabProba);
                ;
            }
        }

        ArrayList<ArrayList<EventResult>> resultatParSource0_4 = new ArrayList<ArrayList<EventResult>>();
        ArrayList<ArrayList<EventResult>> resultatParSource4_8 = new ArrayList<ArrayList<EventResult>>();

        // int MAX_NUMBER_OF_CANDIDATE = 4; // was 4

        {
            for (int nbEventTarget = 0; nbEventTarget < nbTotalDeCritere; nbEventTarget++)
            {
                ArrayList<EventResult> eventResultList = new ArrayList<EventResult>();
                for (int nbEventSource = 0; nbEventSource < nbTotalDeCritere; nbEventSource++)
                {
                    JCheckBox eTarget = selectedEventHashMap.get(nbEventTarget);
                    JCheckBox eSource = selectedEventHashMap.get(nbEventSource);
                    if (!eTarget.isSelected() || !eSource.isSelected())
                        continue;

                    /*
                     * if ( nbEventTarget == 1 || nbEventSource == 1 ) continue;
                     * if ( nbEventTarget == 7 || nbEventSource == 7 ) continue;
                     * if ( nbEventTarget == 8 || nbEventSource == 8 ) continue;
                     * if ( nbEventTarget == 9 || nbEventSource == 9 ) continue;
                     * if ( nbEventTarget == 10 || nbEventSource == 10 ) continue;
                     * if ( nbEventTarget == 13 || nbEventSource == 13 ) continue;
                     * if ( nbEventTarget == 14 || nbEventSource == 14 ) continue;
                     * if ( nbEventTarget == 15 || nbEventSource == 15 ) continue;
                     * if ( nbEventTarget == 16 || nbEventSource == 16 ) continue;
                     * if ( nbEventTarget == 17 || nbEventSource == 17 ) continue;
                     * if ( nbEventTarget == 18 || nbEventSource == 18 ) continue;
                     * if ( nbEventTarget == 19 || nbEventSource == 19 ) continue;
                     * if ( nbEventTarget == 20 || nbEventSource == 20 ) continue;
                     * if ( nbEventTarget >= 27 || nbEventSource >= 27 ) continue;
                     */

                    float probaProposee = meanAnimalProba0_4[nbEventSource][nbEventTarget];
                    float stddevPropose = stddevAnimalProba0_4[nbEventSource][nbEventTarget];

                    ArrayList<EventResult> eventResultListClone = (ArrayList<EventResult>) eventResultList.clone();

                    for (EventResult resultClient : eventResultListClone)
                    {
                        if (resultClient.proba < probaProposee)
                        {
                            eventResultList.remove(resultClient);
                            break;
                        }
                    }

                    if (eventResultList.size() < MAX_NUMBER_OF_CANDIDATE)
                    {
                        EventResult result = new EventResult(nbEventSource, nbEventTarget, probaProposee, stddevPropose,
                                "red");
                        eventResultList.add(result);
                    }

                }
                resultatParSource0_4.add(eventResultList);

            }
        }

        {

            for (int nbEventTarget = 0; nbEventTarget < nbTotalDeCritere; nbEventTarget++)
            {
                ArrayList<EventResult> eventResultList = new ArrayList<EventResult>();
                for (int nbEventSource = 0; nbEventSource < nbTotalDeCritere; nbEventSource++)
                {
                    JCheckBox eTarget = selectedEventHashMap.get(nbEventTarget);
                    JCheckBox eSource = selectedEventHashMap.get(nbEventSource);
                    if (!eTarget.isSelected() || !eSource.isSelected())
                        continue;
                    //
                    // if ( nbEventTarget == 1 || nbEventSource == 1 ) continue;
                    // if ( nbEventTarget == 7 || nbEventSource == 7 ) continue;
                    // if ( nbEventTarget == 8 || nbEventSource == 8 ) continue;
                    // if ( nbEventTarget == 9 || nbEventSource == 9 ) continue;
                    // if ( nbEventTarget == 10 || nbEventSource == 10 ) continue;
                    // if ( nbEventTarget == 13 || nbEventSource == 13 ) continue;
                    // if ( nbEventTarget == 14 || nbEventSource == 14 ) continue;
                    // if ( nbEventTarget == 15 || nbEventSource == 15 ) continue;
                    // if ( nbEventTarget == 16 || nbEventSource == 16 ) continue;
                    // if ( nbEventTarget == 17 || nbEventSource == 17 ) continue;
                    // if ( nbEventTarget == 18 || nbEventSource == 18 ) continue;
                    // if ( nbEventTarget == 19 || nbEventSource == 19 ) continue;
                    // if ( nbEventTarget == 20 || nbEventSource == 20 ) continue;
                    // if ( nbEventTarget >= 27 || nbEventSource >= 27 ) continue;

                    float probaProposee = meanAnimalProba4_8[nbEventSource][nbEventTarget];
                    float stddevPropose = stddevAnimalProba4_8[nbEventSource][nbEventTarget];

                    ArrayList<EventResult> eventResultListClone = (ArrayList<EventResult>) eventResultList.clone();

                    for (EventResult resultClient : eventResultListClone)
                    {
                        if (resultClient.proba < probaProposee)
                        {
                            eventResultList.remove(resultClient);
                            break;
                        }
                    }

                    if (eventResultList.size() < MAX_NUMBER_OF_CANDIDATE)
                    {
                        EventResult result = new EventResult(nbEventSource, nbEventTarget, probaProposee, stddevPropose,
                                "green");
                        eventResultList.add(result);
                    }

                }
                resultatParSource4_8.add(eventResultList);

            }
        }

        // GRAPH:

        boolean FORCE_LOCATION = forceLocation.isSelected();

        System.out.println("Put the following script in GraphViz:");
        if (FORCE_LOCATION)
        {
            System.out.println("/* @command = neato **/");
        }
        System.out.println("digraph  {");
        System.out.println("splines=true;");

        // set the location of each available event

        if (FORCE_LOCATION)
        {
            double x = 0;
            double y = 0;
            double angle = 0;

            for (int eventIndex = 0; eventIndex < nbTotalDeCritere; eventIndex++)
            {
                JCheckBox eventBox = selectedEventHashMap.get(eventIndex);
                if (!eventBox.isSelected())
                    continue;
                //
                // if( eventIndex == 1 ) continue;
                // if( eventIndex == 7 ) continue;
                // if( eventIndex == 8 ) continue;
                // if( eventIndex == 9 ) continue;
                // if( eventIndex == 10 ) continue;
                // if( eventIndex == 13 ) continue;
                // if( eventIndex == 14 ) continue;
                // if( eventIndex == 15 ) continue;
                // if( eventIndex == 16 ) continue;
                // if( eventIndex == 17 ) continue;
                // if( eventIndex == 18 ) continue;
                // if( eventIndex == 19 ) continue;
                // if( eventIndex == 20 ) continue;
                // if( eventIndex >= 27 ) continue;

                x = 300 + Math.cos(angle) * 300;
                y = 300 + Math.sin(angle) * 300;

                String criteriaName = animalList.get(0).animal.eventTimeLineList.get(eventIndex).criteriaName;
                // criteriaName = keepFirstChars(criteriaName);

                String result = "\"" + criteriaName + "\"";
                result += "[";
                result += " pos=\"" + x + "," + y + "!" + "\" ";
                result += " shape=\"box\" ";
                result += " width=\"2\" ";
                result += "]";

                System.out.println(result);

                angle += (Math.PI * 2d) / 14d;

                // x+=200;
                // if ( x > 100 )
                // {
                // y+=100;
                // x=0;
                // }

            }

        }

        // animalList.get( 0 ).animal.eventTimeLineList.get( eventSource ).criteriaName

        // create graph

        for (int sourceList = 0; sourceList < nbTotalDeCritere; sourceList++)
        {
            ArrayList<EventResult> eventResultList0_4 = resultatParSource0_4.get(sourceList);
            ArrayList<EventResult> eventResultList4_8 = resultatParSource4_8.get(sourceList);

            ArrayList<EventResult> finalResult = createCommonList(eventResultList0_4, eventResultList4_8);

            for (EventResult eventResult : finalResult)
            {
                if (eventResult.enable)
                {
                    if (eventResult.proba >= 0.1f) // was 0.3f
                    {
                        System.out.println(eventResult);
                    }
                    else
                    {

                    }
                }
            }

        }

        System.out.println("}");

        // xls.SaveAndClose();

    }

    ArrayList<EventResult> createCommonList(ArrayList<EventResult> eventResultList0_4,
            ArrayList<EventResult> eventResultList4_8)
    {
        ArrayList<EventResult> finalResult = new ArrayList<EventResult>();
        for (EventResult eventResult0_4 : eventResultList0_4)
        {
            finalResult.add(eventResult0_4);
        }
        for (EventResult eventResult4_8 : eventResultList4_8)
        {
            finalResult.add(eventResult4_8);
        }

        for (EventResult eventResultA : finalResult)
        {
            for (EventResult eventResultB : finalResult)
            {
                if (eventResultA != eventResultB && eventResultA.enable && eventResultB.enable
                        && eventResultA.eventSource == eventResultB.eventSource
                        && eventResultA.eventTarget == eventResultB.eventTarget
                        && checkIfProbaOverlap(eventResultA, eventResultB))
                {
                    eventResultA.proba = (eventResultA.proba + eventResultB.proba) / 2f;
                    eventResultB.enable = false;
                    eventResultA.color = "black";
                }

            }
        }

        return finalResult;
    }

    private boolean checkIfProbaOverlap(EventResult eventResultA, EventResult eventResultB)
    {
        float sigma = 1f;

        float borneMinA = eventResultA.proba - eventResultA.stddev * sigma;
        float borneMaxA = eventResultA.proba + eventResultA.stddev * sigma;

        float borneMinB = eventResultB.proba - eventResultB.stddev * sigma;
        float borneMaxB = eventResultB.proba + eventResultB.stddev * sigma;

        if (borneMinA > borneMinB && borneMinA < borneMaxB)
            return true;
        if (borneMaxA > borneMinB && borneMaxA < borneMaxB)
            return true;

        return false;
    }

    class ProbaResult
    {
        public ProbaResult(float proba, float stddev, int startFrame)
        {
            this.stddev = stddev;
            this.proba = proba;
            this.startFrame = startFrame;
        }

        float stddev;
        float proba;
        int startFrame;
    }

    class EventResult
    {
        boolean enable = true;

        public EventResult(int eventSource, int eventTarget, float proba, float stddev, String color)
        {
            this.eventSource = eventSource;
            this.eventTarget = eventTarget;
            this.stddev = stddev;
            this.proba = proba;
            this.color = color;
        }

        float stddev;
        int eventSource;
        int eventTarget;
        float proba;
        String color;

        @Override
        public String toString()
        {

            String result = "";
            // result += "\"" + keepFirstChars(animalList.get(0).animal.eventTimeLineList.get(eventSource).criteriaName)
            result += "\"" + animalList.get(0).animal.eventTimeLineList.get(eventSource).criteriaName + "\"";
            result += "->";
            // result += "\"" + keepFirstChars(animalList.get(0).animal.eventTimeLineList.get(eventTarget).criteriaName)
            result += "\"" + animalList.get(0).animal.eventTimeLineList.get(eventTarget).criteriaName + "\"";
            result += " ";
            result += "[ ";
            result += "color=\"" + color + "\"";
            result += " style=\"setlinewidth(" + proba * 5 + ")\"]";
            result += ";";

            return result;
        }

        /**
         * Just provide the name of events. Without link.
         * 
         * @return
         */
        public String getEvents()
        {
            String result = "";
            // result += "\"" + keepFirstChars(animalList.get(0).animal.eventTimeLineList.get(eventSource).criteriaName)
            result += "\"" + animalList.get(0).animal.eventTimeLineList.get(eventSource).criteriaName + "\"";
            result += ";";
            // result += "\"" + keepFirstChars(animalList.get(0).animal.eventTimeLineList.get(eventTarget).criteriaName)
            result += "\"" + animalList.get(0).animal.eventTimeLineList.get(eventTarget).criteriaName + "\"";
            result += ";";
            return result;
        }
    }

    public static String keepFirstChars(String s)
    {
        int MAXSIZE = 20;
        if (s.length() > MAXSIZE)
            s = (String) s.substring(0, MAXSIZE);
        return s;
    }

    void exportAllData()
    {
        System.out.println("export all data in pool to excel.");

        // export pool 1

        // String saveFileName = SaveDialog.chooseFile("Export all data to xls file", System.getProperty("user.home") ,"output.xls", ".xls");

        JTextField startTextField = new JTextField("0");
        JTextField endTextField = new JTextField("8");

        ActionDialog actionDialog = new ActionDialog("Select range (in minutes)");
        actionDialog.getMainPanel().setLayout(new BoxLayout(actionDialog.getMainPanel(), BoxLayout.PAGE_AXIS));
        actionDialog.getMainPanel().add(GuiUtil.createLineBoxPanel(new JLabel("start (minutes)"), startTextField));
        actionDialog.getMainPanel().add(GuiUtil.createLineBoxPanel(new JLabel("end (minutes)"), endTextField));
        actionDialog.pack();
        actionDialog.setLocationRelativeTo(null);
        actionDialog.setVisible(true);

        if (actionDialog.isCanceled())
            return;

        float startMinute = Float.parseFloat(startTextField.getText());
        float endMinute = Float.parseFloat(endTextField.getText());

        String saveFileName = SaveDialog.chooseFile("Save xls file", "", "Mice Profiler - all data", ".xls");

        if (saveFileName == null)
            return;
        XlsManager xls = null;

        try
        {

            xls = new XlsManager(saveFileName);
            xls.createNewPage("Data in pool");

        }
        catch (IOException e)
        {

            e.printStackTrace();
        }

        xls.createNewPage("Results");

        int cursorY = 0;

        for (EventSelector eventSelector : eventSelectorList)
        {
            String eventTitle = animalList.get(0).animal.eventTimeLineList.get(eventSelector.eventNumber).criteriaName;

            xls.setLabel(0, cursorY, "Event #" + eventSelector.eventNumber, Colour.YELLOW);
            cursorY++;
            xls.setLabel(0, cursorY, eventTitle);
            cursorY += 2;

            cursorY = exportXLSPool(1, eventSelector, cursorY, xls, startMinute, endMinute);
            cursorY += 3;
            cursorY = exportXLSPool(2, eventSelector, cursorY, xls, startMinute, endMinute);
            cursorY += 4;
        }

        xls.SaveAndClose();

    }

    int exportXLSPool(int pool, EventSelector eventSelector, int cursorY, XlsManager xls, float startMinute,
            float endMinute)
    {
        float halfMinute = (endMinute + startMinute) / 2f;

        xls.setLabel(0, cursorY, "p" + pool);
        // xls.setLabel( 1, cursorY, "nb e 0-4" );
        xls.setLabel(1, cursorY, "nb event from " + startMinute + " to " + halfMinute + " minutes");
        // xls.setLabel( 2, cursorY, "nb e 4-8" );
        xls.setLabel(2, cursorY, "nb event from " + halfMinute + " to " + endMinute + " minutes");
        // xls.setLabel( 3, cursorY, "nb e 0-8" );
        xls.setLabel(3, cursorY, "nb event from " + startMinute + " to " + endMinute + " minutes");

        // xls.setLabel( 4, cursorY, "l e 0-4" );
        xls.setLabel(4, cursorY, "length of event from " + startMinute + " to " + halfMinute + " minutes");
        // xls.setLabel( 5, cursorY, "l e 4-8" );
        xls.setLabel(5, cursorY, "length of event from " + halfMinute + " to " + endMinute + " minutes");
        // xls.setLabel( 6, cursorY, "l e 0-8" );
        xls.setLabel(6, cursorY, "length of event from " + startMinute + " to " + endMinute + " minutes");

        float fps = Float.parseFloat(fpsTextField.getText());

        cursorY++;

        for (AnimalPoolSelector animalSelector : animalList)
        {
            if (pool == 1 && animalSelector.pool1.isSelected() || pool == 2 && animalSelector.pool2.isSelected())
            {
                if (animalSelector.enable.isSelected())
                {
                    xls.setLabel(0, cursorY, animalSelector.animal.animalName);

                    Animal animal = animalSelector.animal;
                    EventTimeLine eventTimeLine = animal.eventTimeLineList.get(eventSelector.eventNumber);

                    // boolean debug = false;

                    // nb event e 0-4 le
                    // if ( debug ) System.out.println("0-4");
                    {
                        int nb = 0;
                        int length = 0;

                        // int maxTime = (int) (4*60*fps);
                        int maxTime = (int) (halfMinute * 60 * fps);
                        int minTime = (int) (startMinute * 60 * fps);

                        for (EventCriteria event : eventTimeLine.eventList)
                        {
                            if (event.startFrame < maxTime && event.startFrame >= minTime)
                            {
                                nb++;
                                length += event.getLength();
                                // if ( debug )
                                // {
                                // System.out.println( eventTimeLine.eventList.indexOf( event ) + ">" + event.getLength() );
                                // }
                            }
                        }
                        xls.setNumber(1, cursorY, nb);
                        xls.setNumber(4, cursorY, length);
                        // if ( debug )
                        // {
                        // System.out.println( "nb event"+nb );
                        // System.out.println( "length event"+length );
                        // }

                    }

                    // nb event e 4-8 le
                    // if ( debug ) System.out.println("4-8");
                    {
                        int nb = 0;
                        int length = 0;

                        // int minTime = (int)(4d*60d*fps);
                        int minTime = (int) (halfMinute * 60d * fps);
                        int maxTime = (int) (endMinute * 60 * fps);
                        for (EventCriteria event : eventTimeLine.eventList)
                        {
                            if (event.startFrame < maxTime && event.startFrame > minTime)
                            {
                                nb++;
                                length += event.getLength();
                                // if ( debug )
                                // {
                                // System.out.println( eventTimeLine.eventList.indexOf( event ) + ">" + event.getLength() );
                                // }
                            }
                        }
                        xls.setNumber(2, cursorY, nb);
                        xls.setNumber(5, cursorY, length);
                        // if ( debug )
                        // {
                        // System.out.println( "nb event"+nb );
                        // System.out.println( "length event"+length );
                        // }

                    }

                    // nb event e 0-8 le
                    // if ( debug ) System.out.println("0-8");
                    {
                        int nb = 0;
                        int length = 0;
                        int minTime = (int) (startMinute * 60d * fps);
                        // int maxTime = (int) (8*60*fps);
                        int maxTime = (int) (endMinute * 60 * fps);
                        for (EventCriteria event : eventTimeLine.eventList)
                        {
                            if (event.startFrame < maxTime && event.startFrame > minTime)
                            {
                                nb++;
                                length += event.getLength();
                                // if ( debug )
                                // {
                                // System.out.println( eventTimeLine.eventList.indexOf( event ) + ">" + event.getLength() );
                                // }
                            }
                        }
                        xls.setNumber(3, cursorY, nb);
                        xls.setNumber(6, cursorY, length);
                        // if ( debug )
                        // {
                        // System.out.println( "nb event"+nb );
                        // System.out.println( "length event"+length );
                        // }

                    }

                    cursorY++;

                }
            }
        }
        return cursorY;
    }

}